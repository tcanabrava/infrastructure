# Config retrieved from Mozzila: https://mozilla.github.io/server-side-tls/ssl-config-generator/
events {
    worker_connections 1024;
}

http {
    access_log /var/log/nginx/access.log;

    autoindex on;

    # Serve internal traffic over HTTP.
    server {
        listen {{private_ip}}:80;

        location / {
            root "{{data_path}}";
        }
    }

    server {

        listen 80 default_server;

        # Redirect all non-internal HTTP traffic to HTTPS, via a 301 Moved
        # permantly response.
        return 301 https://$host$request_uri;
    }

    server {
        location / {
            root "{{data_path}}";
        }

        location ^~ /.well-known/acme-challenge/ {
            alias /var/www/dehydrated/;
            default_type text/plain;
        }

        listen 443 ssl;
        listen [::]:443 ssl;

        # certs sent to the client in SERVER HELLO are concatenated in ssl_certificate
        ssl_certificate {{certs_path}}{{ssl_url}}/fullchain.pem;
        ssl_certificate_key {{certs_path}}{{ssl_url}}/privkey.pem;
        ssl_session_timeout 1d;
        ssl_session_cache shared:SSL:50m;
        ssl_session_tickets off;

        # modern configuration. tweak to your needs.
        ssl_protocols TLSv1.2;
        ssl_ciphers 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256';
        ssl_prefer_server_ciphers on;

        # HSTS (ngx_http_headers_module is required) (15768000 seconds = 6 months)
        add_header Strict-Transport-Security max-age=15768000;

        # OCSP Stapling ---
        # fetch OCSP records from URL in ssl_certificate and cache them
        ssl_stapling on;
        ssl_stapling_verify on;

        ## verify chain of trust of OCSP response using Root CA and Intermediate certs
        #le-root found at: https://letsencrypt.org/certificates/
        ssl_trusted_certificate {{certs_path}}le-root.pem;
    }
}
